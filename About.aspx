﻿<%@ Page Title="Sobre Mi" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="ProyectoDeClase.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Ejercicio correspondiente a la tarea evaluable del Curso de ASP.Net.</h3>
    <div>
        El ejercicio consiste en realizar una app Web que contiene una calculadora, con las siguiente operaciones:<br />
        <ul>
            <li>Sumar</li>
            <li>Restar</li>
            <li>Multiplicar</li>
            <li>Dividir</li>
        </ul>
        <br />
        Consta de dos etiquetas input en las que introducideromos un numero cualquiera.
        <br />
        Un caja de elementos en la que elegiremos la operación que queremos realizar.
        <br />
        Un botón que ejecutará la operación que hemos elegido
        <br />
        Y por último una etiqueta que nos mostrará el resultado de la operación elegida.
    </div>
    
</asp:Content>
