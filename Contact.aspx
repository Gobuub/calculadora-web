﻿<%@ Page Title="Contacto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="ProyectoDeClase.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Si tienes cualquier duda puedes contactar conmigo</h3>

    <address>
        <strong>Support:</strong>   <a href="mailto:ortocadp@gmail.com">ortocadp@gmail.com</a><br />
        
    </address>
</asp:Content>
