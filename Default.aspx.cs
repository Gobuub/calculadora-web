﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProyectoDeClase
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Value1.Text.Length > 0 && Value2.Text.Length > 0)
            {
                float result = 0;
                float value1 = Convert.ToInt32(Value1.Text);
                Console.WriteLine(value1);
                float value2 = Convert.ToInt32(Value2.Text);
                Console.WriteLine(value2);

                switch (Operaciones.SelectedValue)
                {
                    case "SUMAR":
                        result = value1 + value2;
                        break;
                    case "RESTAR":
                        result = value1 - value2;
                        break;
                    case "MULTIPLICAR":
                        result = value1 * value2;
                        break;
                    case "DIVIDIR":
                        result = value1 / value2;
                        break;

                }

                resultLabel.Text = result.ToString();
            }

            else 
            {
                resultLabel.Text = string.Empty;
            }
           
        }

    }
}